var net = require('net');
var mongoose = require('mongoose');
var dotenv = require('dotenv');
var events = require('events');
var Dispositivo = require('./models/Dispositivo');
var Sensor = require('./models/Sensor');
var Evento = require('./models/Evento');
var Medicion = require('./models/Medicion');

var emisorEvento = new events.EventEmitter();

// Load environment variables from .env file
dotenv.load();

mongoose.connect('mongodb://localhost/digitales3', {
  useMongoClient: true,
  /* other options */
});
mongoose.connection.once('open', function() {
  console.log('Conectado con la base de datos MongoDB.');
});
mongoose.connection.on('error', function() {
  console.log('Error al conectar con MongoDB, asegurese de que la db esta corriendo.');
});

// setInterval(function(){
//     var temp = getRandomInt(0,100)
//     var alerta = getRandomInt(0,1)
//     // console.log(temp)
//     emisorEvento.emit('nuevaMedicion', `{"nombreRef": "temp","valor":"${temp}", "alerta":"${alerta?true:false}"}`)
// },2000)
//
// function getRandomInt(min, max) {
//   return Math.floor(Math.random() * (max - min + 1)) + min
// }

var server = net.createServer();
server.on('connection', handleConnection);

server.listen(9000, function() {
  console.log('server listening to %s', JSON.stringify(server.address()) );
});

var enviarAlerta = function(med){
  emisorEvento.emit('nuevaMedicion', JSON.stringify(med))
}

var esAlerta = function(dato, evento){
	var alerta = false;
	// analogico
	evento.condiciones.forEach(function(cond){
		if ( (cond.condicion === '>') && (dato > parseInt(cond.valor)) ) {
			alerta = true;
		}
		if ( (cond.condicion === '<') && (dato < parseInt(cond.valor)) ) {
			alerta = true;
		}
		if ( (cond.condicion === '=') && (dato === parseInt(cond.valor)) ) {
			alerta = true;
		}
	});

	return alerta;
}

function handleConnection(conn) {
  var claves = [];
  var alerta = [];
  var eventos;
  var nombreRef = [];
  var idSensor;

  var remoteAddress = conn.remoteAddress + ':' + conn.remotePort;
  console.log('new client connection from %s', remoteAddress);

  conn.write('hola desde Nodejs\n');

  conn.setEncoding('utf8');

  conn.on('data', onConnData);
  conn.once('close', onConnClose);
  conn.on('error', onConnError);



  function onConnData(d) {
    try {
        dato = JSON.parse(d);
    } catch (e) {
				console.log('DEBE ENVIAR UN OBJETO JSON \n');
				conn.end('DEBE ENVIAR UN OBJETO JSON \n');
				return 0;
    }
		//obtengo un array con las claves de json {'clave': 'valor'}
		claves = Object.keys(dato);
		claves.forEach(function(clave, i){
			nombreRef[i] = false;
		});
		// console.log('dato recibido: '+ JSON.stringify(dato) );
		// console.log('claves: '+ claves);
		// console.log('nombreRef: '+ nombreRef);

    if ( !('llave' in dato) ) {
      // console.log('LLAVE NO ENVIADA \n');
      conn.end('LLAVE NO ENVIADA \n');
			conn.destroy();
    }else{
      Dispositivo
      .findOne({ llave: dato.llave })
      .populate({
        path: 'eventos',
        populate: { path: 'idSensor' }
      })
      .exec(function (err, consulta) {
        if (err) return handleError(err);
        //si encuentra el dispositivo, lo guardo en dispositivo
        if (consulta) {
					// console.log('dispositivos => ' +consulta);
          eventos = consulta.eventos;
          // console.log('eventos => ' + eventos);


          Sensor
						.find({idDispositivo: consulta._id})
						.exec(function(err, sensores) {
							if (err) return handleError(err)

              if (sensores) {
                // console.log('sensores => ' + sensores);

                sensores.forEach(function(sensor){
  								claves.forEach(function(clave, i){
  									if (clave == sensor.nombreRef) {

  										// console.log('coinside ' + sensor.nombreRef);
  										nombreRef[i] = true;
  										// console.log(nombreRef);

  										alerta[i] = false;
  										eventos.forEach(function(evento){
  											if (sensor.nombreRef == evento.idSensor.nombreRef) {
  												alerta[i] = esAlerta(dato[clave], evento);
  												// console.log(evento.idSensor.nombreRef + '  - alerta -  ' + alerta[i]);
  											}
  										});

  										var medicion = new Medicion({
  											idSensor: sensor,
  											valor: dato[clave],
  											alerta: alerta[i] ? 'true' : 'false' //fijarse bien
  										});
  										medicion.save().then( function(med){
  										  console.log('alerta ->' + alerta[i]);
                        var envio = med
                        console.log('ENVIO =>' +med);
                        enviarAlerta(med)
  											// enviarAlerta(med, alerta[i])
  										})
  									}
  								})
  							})


                console.log('OK \n');
                conn.end('OK \n');
                conn.destroy();
              }else{
                console.log('NO HAY SENSOR \n');
                conn.end('NO HAY SENSOR \n');
                conn.destroy();
              }

            });

        }else{
          console.log('NO HAY DISPOSITIVO \n');
          conn.end('NO HAY DISPOSITIVO \n');
          conn.destroy();
        }
      });
    }
  }

  function onConnClose() {
    console.log('connection from %s closed', remoteAddress);
  }

  function onConnError(err) {
    console.log('Connection %s error: %s', remoteAddress, err.message);
    conn.write(err);
  }
}

module.exports = emisorEvento
