const mongoose = require('mongoose');
const Dispositivo = require('./Dispositivo');
const Sensor = require('./Sensor');
const Evento = require('./Evento');
const Medicion = require('./Medicion');
const User = require('./User');



const sensores = [
  { 
    nombre: "Temperatura",
    nombreRef: "temp",
  },
  { 
    nombre: "Humedad",
    nombreRef: "hum", 
  }
]




mongoose.connect('mongodb://localhost/digitales3',  { useUnifiedTopology: true, useNewUrlParser: true });
mongoose.set('useCreateIndex', true)
// get reference to database
var db = mongoose.connection;

db.on('error', console.error.bind(console, 'connection error:'));

db.once('open', function() {
    console.log("Connection Successful!");
    
    // define Schema
    let BookSchema = mongoose.Schema({
      name: String,
      price: Number,
      quantity: Number
    });
 
    // a document instance
    let user = new User({ 
        name: "Admin",
        email: "a@a.com",
        password: "123123"
    });
 
    // creo
    user.save(function (err, user) {
      
      console.log(user.name + " usuario guardado.");

      User.findOne({name: user.name}, function(err, docUser){
        if (err) return console.error(err);

        console.log(docUser);
        
        let dispositivo = new Dispositivo({
          nombre: "Prueba",
          descripcion: "este es un dispositivo de prueba",
          posicion:{
          lat: -33.253690,
          lng: -60.304878
          },
          usuarioReferencia: docUser._id,
          llave: "123123"
        });

        dispositivo.save(function (err, disp) {
          console.log(disp.nombre + " dispositivo guardado.");

          Dispositivo.findOne({name: disp.name}, function(err, docDisp){
            if (err) return console.error(err);

            console.log(docDisp);

            sensores.map(function(dat){
              let sensor = new Sensor({
                idDispositivo: docDisp._id,
                nombre: dat.nombre,
                nombreRef: dat.nombreRef,
                descripcion: "descripcion"
              });

              sensor.save(function (err, sens){
                console.log(sens.nombre + " sensor guardado.");
              })

            });
            
          });

        });

      });

    });
    
});
 
