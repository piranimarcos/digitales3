#include <SPI.h>
#include <Ethernet.h>
#include <DHT11.h>

byte mac[] = { 0x00,0xAA,0xBB,0xCC,0xDE,0x02 };
// ls mac debe traerse desde un archivo 
IPAddress ip(192,168,1,50);
IPAddress server(192,168,1,2);

EthernetClient client;

char _buffer[256];
char _llave[] = "aSFg5t42";
//char _llave[] = "llave";

unsigned long ultimaConexion = 0;

const unsigned long segundos = 5L; //5 segundos
const unsigned long intervalo = segundos * 1000L;

int pinSensor = 7;
DHT11 dht11(pinSensor);

void setup() {
  Ethernet.begin(mac, ip);
  Serial.begin(9600);

  delay(1000);
  Serial.println("Conectando");
}

void loop(){

  if (millis() - ultimaConexion > intervalo) {
    Request();
    ultimaConexion = millis();
  }

}

void Request() {

    if (client.connect(server, 9000)) {
      Serial.println("Conectado");
    }
    else {
      Serial.println("Conexion Fallida");
    }

    boolean detener = false;

    if (client.available()) {
      String c = client.readString();
      Serial.print(c);
/*
      int A0 = analogRead(A0);
      int A1 = analogRead(A1);
      int A2 = analogRead(A2);
      int A3 = analogRead(A3);
      int A4 = analogRead(A4);
      int A5 = analogRead(A5);
      sprintf(_buffer, "{\"A0\":\"%i\",\"A1\":\"%i\",\"A2\":\"%i\",\"A3\":\"%i\",\"A4\":\"%i\",\"A5\":\"%i\",\"llave\":\"%s\"}", A0,A1,A2,A3,A4,A5, _llave);
*/
       int err;
       float temp, hum;
       if((err = dht11.read(hum, temp)) == 0)    // Si devuelve 0 es que ha leido bien
       {
         Serial.print("Temperatura: ");
         Serial.print(temp);
         Serial.print(" Humedad: ");
         Serial.print(hum);
         Serial.println();
       }else{
         Serial.println();
         Serial.print("Error Num :");
         Serial.print(err);
         Serial.println();
       }
      
      sprintf(_buffer, "{\"temp\":\"%i\",\"hum\":\"%i\",\"llave\":\"%s\"}", (int) temp, (int) hum, _llave);

      client.print(_buffer);

      c = client.readString();
      Serial.print(c);

      detener = true;

    }

    while (Serial.available() > 0) {
      char inChar = Serial.read();
      if (client.connected()) {
        client.print(inChar);
      }
    }

    if (!client.available() || detener) {
      Serial.println("Desconectado");
      client.stop();
    }
}
