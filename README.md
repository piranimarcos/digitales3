## Digitales 3 Problema de Ingeniería

Sistema de monitoreo y manejo de alarmas

### Realizado con
- **Hardware:** arduino
- **Platform:** node
- **Framework**: express
- **CSS Preprocessor**: sass
- **JavaScript Framework**: react
- **Build Tool**: npm
- **Database**: mongodb

### Licencia
The MIT License (MIT)

Copyright (c) 2017 Digitales 3
