var express = require('express')
var app =  express()
var server = require('http').Server(app)
var io = require('socket.io')(server)
var path = require('path')

var mongoose = require('mongoose')
var baucis = require('baucis')

var Dispositivo = require('./models/Dispositivo')
var Evento = require('./models/Evento')
var Sensor = require('./models/Sensor')
var Medicion = require('./models/Medicion')



var connections = 0



server.listen(3000)
console.log('Server listening on port :3000')

mongoose.connect('mongodb://localhost/digitales3', {
  useMongoClient: true,
  /* other options */
})
mongoose.Promise = global.Promise
mongoose.connection.on('error', function() {
  console.log('MongoDB Connection Error. Please make sure that MongoDB is running.')
  process.exit(1)
})

mongoose.connection.once('open', function() {
  console.log('db conectada ✓')
})

app.use(express.static(path.join(__dirname, 'public')))

io.sockets.on('connection', (socket) => {
  console.log('Usuarios conectados --> '+ ++connections)
  socket.on('disconnect', () => {
    console.log('Usuarios conectados --> '+ --connections)
  })
})

var emisorEvento = require('../tcp')

// console.log(emisorEvento);

emisorEvento.on('nuevaMedicion', function(e){
  console.log('listener =>'+ e);
  io.emit('medicion', e);
});

// setInterval(function(){
//     var temp = getRandomInt(0,100)
//     var alerta = getRandomInt(0,1)
//     // console.log(temp)
//     io.sockets.emit('medicion', `{"nombreRef": "temp","valor":"${temp}", "alerta":"${alerta?true:false}"}`)
// },2000)
//
// setInterval(function(){
//     var hum = getRandomInt(0,100)
//     var alerta = getRandomInt(0,1)
//     // console.log(hum)
//     io.sockets.emit('medicion', `{"nombreRef": "hum","valor":"${hum}", "alerta":"${alerta?true:false}"}`)
// },2000)

// function getRandomInt(min, max) {
//   return Math.floor(Math.random() * (max - min + 1)) + min
// }

baucis.rest('Dispositivo')
baucis.rest('Medicion')
baucis.rest('Evento')
baucis.rest('Sensor')

app.use('/api/v1', baucis())

app.get('/', (req, res) => {
  res.sendFile(__dirname+'/index.html')
})

app.get('/historial', (req, res) => {
  res.sendFile(__dirname+'/historial.html')
})
