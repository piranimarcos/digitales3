$(document).ready(function(){

  //creacion de mapa con marcador
  function initialize() {
    var center = new google.maps.LatLng(-33.329529, -60.225836)
    var mapProp = {
      center:center,
      zoom:15,
      scrollwheel: false,
      scaleControl: false,
      mapTypeId:google.maps.MapTypeId.ROADMAP
    };
    var map = new google.maps.Map(document.getElementById("googleMap"),mapProp);
    var marker = new google.maps.Marker({
      position:center,
    });

    marker.setMap(map);
  }
  google.maps.event.addDomListener(window, 'load', initialize);


  //smooth scroll para la landing
  $('a[href*="#"]:not([href="#"])').click(function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
      if (target.length) {
        $('html, body').animate({
          scrollTop: target.offset().top-60
        }, 1000);
        return false;
      }
    }
  });

  //tooltip
  $('[data-toggle="tooltip"]').tooltip()


});
