var mongoose = require('mongoose');

var dispositivoSchema = new mongoose.Schema({
  nombre: {type: String, required: true},
  descripcion: {type: String, required: true},
  posicion:{
   lat: {type: Number},
   lng: {type: Number}
  },
  usuarioReferencia: {type: mongoose.Schema.Types.ObjectId, ref: 'User', required: true},
  eventos: [ {type: mongoose.Schema.Types.ObjectId, ref: 'Evento'}],
  llave: { type: String, required: true }
}, { timestamps: true });

var Dispositivo = mongoose.model('Dispositivo', dispositivoSchema);

module.exports = Dispositivo;
