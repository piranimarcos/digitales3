var mongoose = require('mongoose');

var sensorSchema = new mongoose.Schema({
  idDispositivo: {type: mongoose.Schema.Types.ObjectId, ref: 'Dispositivo', required: true},
  nombre: {type: String, required: true},
  nombreRef: {type: String, required: true},
  descripcion: {type: String, required: true}
}, { timestamps: true });

var Sensor = mongoose.model('Sensor', sensorSchema);

module.exports = Sensor;
