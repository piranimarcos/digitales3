var mongoose = require('mongoose');

var eventoSchema = new mongoose.Schema({
  idSensor: {type: mongoose.Schema.Types.ObjectId, ref: 'Sensor', required: true},
  condiciones: [
    {
      condicion: {type: String, enum: ['<', '=', '>'], required: true},
      valor: {type: String, required: true}
    }
  ]
}, { timestamps: true });

var Evento = mongoose.model('Evento', eventoSchema);

module.exports = Evento;
