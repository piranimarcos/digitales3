var mongoose = require('mongoose');

var medicionSchema = new mongoose.Schema({
  idSensor: {type: mongoose.Schema.Types.ObjectId, ref: 'Sensor', required: true},
  valor: {type: String, required: true},
  alerta: {type: String, required: true},
  alertaVista: {type: Boolean, required: true, default: false}
}, { timestamps: true });

var Medicion = mongoose.model('Medicion', medicionSchema);

module.exports = Medicion;
